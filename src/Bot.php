<?php

namespace Drupal\telegram_bot;

use Exception;

/**
 * Class Bot.
 *
 * @package Drupal\telegram_bot
 */
class Bot {

  protected $token;
  protected $message;

  /**
   * Bot constructor.
   *
   * @param string $token
   *   Bot token.
   */
  public function __construct($token) {
    $this->setToken($token);
  }

  /**
   * Telegram API request.
   *
   * @param string $method
   *   Method.
   * @param array $params
   *   Params.
   *
   * @return bool|array Response.
   *   Response.
   *
   * @throws \Exception
   */
  private function apiRequest($method, $params = array()) {
    if (!is_string($method)) {
      error_log("Method name must be a string\n");
      return FALSE;
    }

    if (!$params) {
      $params = array();
    }
    else {
      if (!is_array($params)) {
        error_log("Parameters must be an array\n");
        return FALSE;
      }
    }

    $url = 'https://api.telegram.org/bot' . $this->getToken() . '/' . $method;

    $handle = curl_init($url);

    curl_setopt($handle, CURLOPT_POST, TRUE);
    curl_setopt($handle, CURLOPT_POSTFIELDS, $params);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);

    $response = curl_exec($handle);

    if ($response === FALSE) {
      $errno = curl_errno($handle);
      $error = curl_error($handle);
      error_log("Curl returned error $errno: $error\n");
      curl_close($handle);
      return FALSE;
    }

    $http_code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
    curl_close($handle);

    if ($http_code >= 500) {
      // We wouldn't want to DDOS the server if something goes wrong.
      sleep(10);
      return FALSE;
    }
    else {
      if ($http_code != 200) {
        $response = json_decode($response, TRUE);
        error_log("Request has failed with error {$response['error_code']}: {$response['description']}\n");
        if ($http_code == 401) {
          throw new Exception('Invalid access token provided');
        }
        return FALSE;
      }
      else {
        $response = json_decode($response, TRUE);
        if (isset($response['description'])) {
          error_log("Request was successful: {$response['description']}\n");
        }
        $response = $response['result'];
      }
    }

    return $response;
  }

  /**
   * Send message.
   *
   * @param int|string $chat_id
   *   Chat ID.
   * @param string $text
   *   Text.
   *
   * @return bool|array
   *   Response.
   */
  public function sendMessage($chat_id, $text) {
    return self::apiRequest('sendMessage', array(
      'chat_id' => $chat_id,
      'text' => $text,
    ));
  }

  /**
   * Bot API method geMe().
   *
   * @return bool|array
   *   Response.
   */
  public function getMe() {
    return $this->apiRequest('getMe');
  }

  /**
   * Bot API method setWebhook().
   *
   * @param string $url
   *   HTTPS url to send updates to. Use an empty string to remove webhook integration.
   * @param string $certificate
   *   Upload your public key certificate so that the root certificate in use can be checked.
   *
   * @return bool|array
   *   State.
   */
  public function setWebhook($url = '', $certificate = NULL) {

    if (!is_null($certificate) || !empty($certificate)) {
      $certificate = new \CURLFile($certificate);
    }
    $response = $this->apiRequest('setWebhook', [
      'url' => $url,
      'certificate' => $certificate,
    ]);

    return $response;
  }

  /**
   * GETTERS / SETTERS.
   */

  /**
   * Get bot token.
   *
   * @return string
   *   Bot token.
   */
  public function getToken() {
    return $this->token;
  }

  /**
   * Set bot token.
   *
   * @param string $token
   *   Bot token.
   *
   * @return $this
   */
  public function setToken($token) {
    $this->token = $token;
    return $this;
  }

  /**
   * Bot API method getUpdates().
   *
   * @return array|bool
   *   Response.
   */
  public function getUpdates() {
    return $this->apiRequest('getUpdates');
  }

}
