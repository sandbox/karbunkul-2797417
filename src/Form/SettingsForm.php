<?php

namespace Drupal\telegram_bot\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\telegram_bot\Bot;

/**
 * Class SettingsFrom.
 *
 * @package Drupal\telegram_bot\Form
 */
class SettingsForm extends ConfigFormBase {

  const CONFIG_SETTINGS = 'telegram_bot.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'telegram_bot_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    error_reporting(E_ALL);

    $form['#prefix'] = '<div id="telegram-bot-settings-form-wrapper">';
    $form['#suffix'] = '</div>';

    /** @var Config $config */
    $config = $this->config(self::CONFIG_SETTINGS);

    $is_web_hook_support = $this->isWebHookSupport();

    $form['token'] = [
      '#type' => 'password',
      '#title' => $this->t('Bot token'),
      '#required' => TRUE,
      '#attributes' => array('value' => $config->get('token')),
      '#description' => $this->t('Create bot from telegram @BotFather'),
    ];

    $https_warning = $this->t('Warning! <strong>HTTPS</strong> protocol is required for telegram bot API method setWebhook()');

    $form['webhook'] = [
      '#type' => 'radios',
      '#title' => $this->t('Set up webhook'),
      '#options' => [
        0 => $this->t('Deactivate webhook'),
        1 => $this->t('Activate webhook by self-signed certificate'),
        2 => $this->t('Activate webhook without certificate'),
      ],
      '#required' => TRUE,
      '#description' => (!$is_web_hook_support) ? $https_warning : '',
      '#default_value' => $config->get('webhook'),
      '#disabled' => !$is_web_hook_support,
    ];

    $form['certificate'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Self-signed certificate pem file'),
      '#states' => [
        'visible' => [
          ':input[name="webhook"]' => array('value' => 1),
        ],
      ],
    ];

    $form['certificate']['file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload PEM file'),
      '#default_value' => $config->get('certificate'),
      '#upload_location' => 'public://telegram-bot',
      '#upload_validators' => [
        'file_validate_extensions' => ['pem cer'],
      ],
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    //dsm($form);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_SETTINGS);
    $token = $form_state->getValue('token');
    //$config->clear('token')->save();

    // If new token.
    if ($config->get('token') !== $token) {
      $bot = new Bot($token);
      $me = $bot->getMe();
      if (is_array($me)) {
        $config->set('id', $me['id'])->save();
        $config->set('username', $me['username'])->save();
        $config->set('token', $token)->save();
        drupal_set_message($this->t('Bot @!bot is active now.', ['!bot' => $me['username']]));
      }
      else {
        $form_state->setErrorByName('token', $this->t('Invalid bot token.'));
        return FALSE;
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_SETTINGS);

//    $bot = new Bot($form_state->getValue('token'));
//    dsm($bot->getUpdates());

    $this->saveCertificate($form_state, $config);
    $this->setupWebhook($form_state, $config);

    parent::submitForm($form, $form_state);
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [
      self::CONFIG_SETTINGS,
    ];
  }

  /**
   * Check if site support web hook.
   *
   * @return bool
   *   Check state.
   */
  private function isWebHookSupport() {
    return ($this->getRequest()->getScheme() == 'http') ? FALSE : TRUE;
  }

  /**
   * Save certificate logic.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param \Drupal\Core\Config\Config $config
   *   Bot settings config.
   */
  private function saveCertificate(FormStateInterface $form_state, Config $config) {
    $file_id = $form_state->getValue('file');
    $old_certificate_fid = $config->get('certificate');
    if ($file_id !== $old_certificate_fid) {
      $file = File::load($file_id[0]);
      $file->setPermanent();
      $file->save();
      \Drupal::service('file.usage')
             ->add($file, 'telegram_bot', 'file', $file_id[0]);
      $config->get('certificate');
      if ($old_file = File::load($old_certificate_fid[0])) {
        \Drupal::service('file.usage')
               ->delete($file, 'telegram_bot', 'file', $old_certificate_fid[0]);
        $old_file->delete();
      }

      $config->set('certificate', $file_id)->save();
    }
  }

  /**
   * Set up webhook.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param \Drupal\Core\Config\Config $config
   *   Bot settings config.
   */
  private function setupWebhook(FormStateInterface $form_state, Config $config) {
    $token = $form_state->getValue('token');
    $webhook = $form_state->getValue('webhook');
    $bot = new Bot($token);

    $webhook_url = $this->getUrlGenerator()
      ->generateFromRoute('telegram_bot.webhook',
        ['token' => $token], ['absolute' => TRUE]);

    $webhook_certificate = '';

    if ($webhook == 1) {
      $certificate_fid = $config->get('certificate');
      if ($certificate = File::load($certificate_fid[0])) {
        if ($file = \Drupal::service('file_system')->realpath($certificate->getFileUri())) {
          if ($content = file_get_contents($file)) {
            $webhook_certificate = $file;
          }
        }
      }
    }

    switch ($webhook) {
      case 0:
        $bot->setWebhook('');
        break;

      case 1:
      case 2:
        //dsm($webhook_url);
        $res = $bot->setWebhook($webhook_url, $webhook_certificate);
        dsm($res);
        break;
    }

    $config->set('webhook', $webhook)->save();

  }

}
