<?php

namespace Drupal\telegram_bot;

/**
 * Interface CommandInterface.
 *
 * @package Drupal\telegram_bot
 */
interface CommandInterface {

  /**
   * Command name.
   *
   * @return string
   *   Command name.
   */
  public function getName();

  /**
   * Command description.
   *
   * @return string
   *   Command description.
   */
  public function getDescription();

  /**
   * Process message.
   *
   * @param string $message
   *   Message.
   *
   * @return mixed
   */
  public function processMessage($message);

}
