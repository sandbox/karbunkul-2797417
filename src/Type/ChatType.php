<?php

namespace Drupal\telegram_bot\Type;

/**
 * Class ChatType.
 *
 * @package Drupal\telegram_bot\Type
 */
class ChatType {

  protected $id;
  protected $type;
  protected $title;
  protected $username;
  protected $firstName;
  protected $lastName;

  /**
   * Get chat id.
   *
   * @return int
   *   Unique identifier for this chat.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set chat id.
   *
   * @param int $id
   *   Unique identifier for this chat.
   *
   * @return $this
   */
  public function setId($id) {
    $this->id = $id;
    return $this;
  }

  /**
   * Get chat type.
   *
   * @return string
   *   Type of chat, can be either private, group, supergroup or channel.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Set chat type.
   *
   * @param string $type
   *   Type of chat, can be either private, group, supergroup or channel.
   *
   * @return $this
   */
  public function setType($type) {
    $this->type = $type;
    return $this;
  }

  /**
   * Get title.
   *
   * @return string
   *   Title, for channels and group chats.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Set title.
   *
   * @param string $title
   *   Title, for channels and group chats.
   *
   * @return $this
   */
  public function setTitle($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * Get username.
   *
   * @return string
   *   Username, for private chats, supergroups and channels if available.
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * Set username.
   *
   * @param string $username
   *   Username, for private chats, supergroups and channels if available.
   *
   * @return $this
   */
  public function setUsername($username) {
    $this->username = $username;
    return $this;
  }

  /**
   * Get first name.
   *
   * @return string
   *   First name of the other party in a private chat.
   */
  public function getFirstName() {
    return $this->firstName;
  }

  /**
   * Set first name.
   *
   * @param string $first_name
   *   First name of the other party in a private chat.
   *
   * @return $this
   */
  public function setFirstName($first_name) {
    $this->firstName = $first_name;
    return $this;
  }

  /**
   * Get last name.
   *
   * @return string
   *   Last name of the other party in a private chat.
   */
  public function getLastName() {
    return $this->lastName;
  }

  /**
   * Set last name.
   *
   * @param string $last_name
   *   Last name of the other party in a private chat.
   *
   * @return $this
   */
  public function setLastName($last_name) {
    $this->lastName = $last_name;
    return $this;
  }

}
