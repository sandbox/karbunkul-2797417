<?php

namespace Drupal\telegram_bot\Type;

/**
 * Class AudioType.
 *
 * @package Drupal\telegram_bot\Type
 */
class AudioType {

  protected $fileId;
  protected $duration;
  protected $performer;
  protected $title;
  protected $mimeType;
  protected $fileSize;

  /**
   * GETTERS / SETTERS.
   */

  /**
   * Get file id.
   *
   * @return string
   *   Unique identifier for this file.
   */
  public function getFileId() {
    return $this->fileId;
  }

  /**
   * Set file id.
   *
   * @param string $file_id
   *   Unique identifier for this file.
   *
   * @return $this
   */
  public function setFileId($file_id) {
    $this->fileId = $file_id;
    return $this;
  }

  /**
   * Get duration.
   *
   * @return int
   *   Duration of the audio in seconds as defined by sender.
   */
  public function getDuration() {
    return $this->duration;
  }

  /**
   * Get duration.
   *
   * @param mixed $duration
   *   Duration of the audio in seconds as defined by sender.
   *
   * @return $this
   */
  public function setDuration($duration) {
    $this->duration = $duration;
    return $this;
  }

  /**
   * Get performer.
   *
   * @return string
   *   Performer of the audio as defined by sender or by audio tags.
   */
  public function getPerformer() {
    return $this->performer;
  }

  /**
   * Set performer.
   *
   * @param mixed $performer
   *   Performer of the audio as defined by sender or by audio tags.
   *
   * @return $this
   */
  public function setPerformer($performer) {
    $this->performer = $performer;
    return $this;
  }

  /**
   * Get title.
   *
   * @return string
   *   Title of the audio as defined by sender or by audio tags.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Get title.
   *
   * @param mixed $title
   *   Title of the audio as defined by sender or by audio tags.
   *
   * @return $this
   */
  public function setTitle($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * Get mime-type.
   *
   * @return string
   *   MIME type of the file as defined by sender.
   */
  public function getMimeType() {
    return $this->mimeType;
  }

  /**
   * Set mime-type.
   *
   * @param mixed $mime_type
   *   MIME type of the file as defined by sender.
   *
   * @return $this
   */
  public function setMimeType($mime_type) {
    $this->mimeType = $mime_type;
    return $this;
  }

  /**
   * Get file size.
   *
   * @return int
   *   File size.
   */
  public function getFileSize() {
    return $this->fileSize;
  }

  /**
   * Set file size.
   *
   * @param int $file_size
   *   File size.
   *
   * @return $this
   */
  public function setFileSize($file_size) {
    $this->fileSize = $file_size;
    return $this;
  }

}
