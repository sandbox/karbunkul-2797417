<?php

namespace Drupal\telegram_bot\Type;

/**
 * Class UserType.
 *
 * @package Drupal\telegram_bot\Type
 */
class UserType {

  protected $id;
  protected $firstName;
  protected $lastName;
  protected $username;

  /**
   * UserType constructor.
   *
   * @param int $id
   *   Unique identifier for this user or bot.
   * @param string $first_name
   *   User‘s or bot’s first name.
   */
  public function __construct($id, $first_name) {
    $this->setId($id)->setFirstName($first_name);
  }

  /**
   * GETTERS / SETTERS.
   */

  /**
   * Get user id.
   *
   * @return int
   *   Unique identifier for this user or bot.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set user id.
   *
   * @param int $id
   *   Unique identifier for this user or bot.
   *
   * @return $this
   */
  public function setId($id) {
    $this->id = $id;
    return $this;
  }

  /**
   * Get first name.
   *
   * @return string
   *   User‘s or bot’s first name.
   */
  public function getFirstName() {
    return $this->firstName;
  }

  /**
   * Set first name.
   *
   * @param string $first_name
   *   User‘s or bot’s first name.
   *
   * @return $this
   */
  public function setFirstName($first_name) {
    $this->firstName = $first_name;
    return $this;
  }

  /**
   * Get last name.
   *
   * @return string
   *   User‘s or bot’s last name.
   */
  public function getLastName() {
    return $this->lastName;
  }

  /**
   * Set last name.
   *
   * @param string $last_name
   *   User‘s or bot’s last name.
   *
   * @return $this
   */
  public function setLastName($last_name) {
    $this->lastName = $last_name;
    return $this;
  }

  /**
   * Get username.
   *
   * @return string
   *   User‘s or bot’s username.
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * Set username.
   *
   * @param string $username
   *   User‘s or bot’s username.
   *
   * @return $this
   */
  public function setUsername($username) {
    $this->username = $username;
    return $this;
  }

}
