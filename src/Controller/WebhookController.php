<?php

namespace Drupal\telegram_bot\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WebhookController.
 *
 * @package Drupal\telegram_bot\Controller
 */
class WebhookController extends ControllerBase {

  /**
   * Webhook.
   *
   * @param string $token
   *   Bot token.
   *
   * @return string Data updates.
   *   Data updates.
   */
  public function webhook($token) {

    $config = $this->config('telegram_bot.settings');
    $saved_token = $config->get('token');
    $response = new Response();
    if ($saved_token == $token) {
      $updates = json_decode(file_get_contents('php://input'));
      $out = json_encode($updates);
      //$this->loggerFactory->get('telegram_bot')->debug($out);
      \Drupal::logger('telegram_bot')->notice($out);
    }
    else {
      $out = 'invalid token';
    }

    $content = [
      '#markup' => $out,
    ];

    $response->setContent(render($content));
    $response->headers->set('Content-Type', 'text/plain charset=UTF-8');
    return $response;
  }

}
